# HalAN-microservices-levels

Microservices for retrieve level of the game from mongoDB repo

## Getting Started

Clone the code from repo

`mvn clean install`

Get the jar from the target folder and run it with command

`java -jar microserices-level-SNAPSHOT`
### Prerequisites

What things you need to install the software and install them

```
maven
java8
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Documentation for testing api is available on [Documentation](http://localhost:8080/swagger-ui.html)

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring Boot](https://spring.io/projects/spring-boot) - Depency Injection Framework

## Versioning

We use [Gitlab](http://gitlab.com/) for versioning.

## Authors

* **Salvatore Chiariello** - *Initial work* - [signorpiero](https://github.com/signorpiero)
* **Carmine Parisi** - *Initial work* - [pakkiotto](https://github.com/pakkiotto)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
