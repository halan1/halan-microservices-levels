package it.halan.microserviceslevels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicesLevelsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicesLevelsApplication.class, args);
	}

}
