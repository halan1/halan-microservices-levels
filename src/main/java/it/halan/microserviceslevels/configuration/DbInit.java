package it.halan.microserviceslevels.configuration;

import it.halan.microserviceslevels.models.Category;
import it.halan.microserviceslevels.models.Level;
import it.halan.microserviceslevels.repository.CategoryRepository;
import it.halan.microserviceslevels.repository.LevelRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
@AllArgsConstructor
@EnableMongoAuditing
public class DbInit implements CommandLineRunner {

    private final LevelRepository levelRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public void run(String... args) throws Exception {

        log.info("Start init of database embedded");

        Category category1 = Category.builder()
                .name("music")
                .build();

        Category category2 = Category.builder()
                .name("film")
                .build();

        categoryRepository.saveAll(Arrays.asList(category1,category2));

        Level level1 = Level.builder()
                .difficult("1")
                .encrypt("C**ZO M*N* A M*")
                .solution("CAZZO MENE A ME")
                .description("Placeholder per la descrizione")
                .category(category1)
                .build();
        Level level2 = Level.builder()
                .difficult("1")
                .encrypt("C**ISS*M* AM*C* A M*")
                .solution("CARISSSIMI AMICI A ME")
                .description("Placeholder per la descrizione")
                .category(category2)
                .build();

        List<Level> levels = Arrays.asList(level1,level2);

        log.info("Levels inserted in the mongo repository: {}",levelRepository.saveAll(levels));

    }
}
