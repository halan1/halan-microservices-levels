package it.halan.microserviceslevels.models;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;
import java.time.LocalDateTime;

@Document(collection = "levels")
@Data
@Builder
public class Level{
    String id;
    String description;
    String solution;
    String encrypt;
    String difficult;
    @DBRef
    Category category;
    @CreatedDate
    private java.util.Date createdDate;
    @LastModifiedDate
    private java.util.Date lastModifiedDate;
}
