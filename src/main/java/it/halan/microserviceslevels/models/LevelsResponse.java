package it.halan.microserviceslevels.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class LevelsResponse {
    int size;
    List<Level> levels;
    java.util.Date lastModified;
}
