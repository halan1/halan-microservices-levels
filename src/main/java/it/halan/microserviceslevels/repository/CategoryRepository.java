package it.halan.microserviceslevels.repository;

import it.halan.microserviceslevels.models.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository  extends MongoRepository<Category, String> {
}
