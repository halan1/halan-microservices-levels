package it.halan.microserviceslevels.repository;

import it.halan.microserviceslevels.models.Level;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LevelRepository extends MongoRepository<Level, String> {

    Optional<Level> findByCategoryId(String categoryId);

}
