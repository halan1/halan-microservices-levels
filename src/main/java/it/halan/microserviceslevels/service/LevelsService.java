package it.halan.microserviceslevels.service;

import it.halan.microserviceslevels.models.Level;
import it.halan.microserviceslevels.models.LevelsResponse;
import it.halan.microserviceslevels.repository.LevelRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@AllArgsConstructor
public class LevelsService {

    private final LevelRepository levelRepository;

    public LevelsResponse getLevels(){
      List<Level> levels = levelRepository.findAll();

      return buildResponse(levels);
    }

    public LevelsResponse getLevelById(String id){
        Optional<Level> level = levelRepository.findById(id);

        List<Level> levels = level.isPresent() ? Arrays.asList(level.get()) : Collections.emptyList();

        return buildResponse(levels);
    }

    public LevelsResponse getLevelByCategoryId(String categoryId){
        Optional<Level> level = levelRepository.findByCategoryId(categoryId);

        List<Level> levels = level.isPresent() ? Arrays.asList(level.get()) : Collections.emptyList();

        return buildResponse(levels);
    }

    public boolean deleteLevelById(String id){

        if(levelRepository.existsById(id)){
            levelRepository.deleteById(id);
            return true;
        }

        return false;
    }

    public LevelsResponse insertLevel(Level level) {
        Level levelSaved = levelRepository.insert(level);

        if (levelSaved != null) {
            return buildResponse(Arrays.asList(levelSaved));
        }
        return LevelsResponse.builder()
                .size(0)
                .levels(Collections.emptyList())
                .build();
    }

    private LevelsResponse buildResponse(List<Level> levels){

        java.util.Date lastUpdate = levels.stream()
                .map(Level::getLastModifiedDate)
                .max(Date::compareTo)
                .orElseGet(null);

        return LevelsResponse.builder()
                .size(levels.size())
                .levels(levels)
                .lastModified(lastUpdate)
                .build();
    }
}
