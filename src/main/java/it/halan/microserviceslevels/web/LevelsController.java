package it.halan.microserviceslevels.web;

import it.halan.microserviceslevels.models.Level;
import it.halan.microserviceslevels.models.LevelsResponse;
import it.halan.microserviceslevels.service.LevelsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/levels")
public class LevelsController {

    private final LevelsService levelsService;

    @GetMapping("")
    public ResponseEntity<LevelsResponse> getAllLevels(@RequestParam(required=false) String lastUpdate){
        log.info("Request all levels from database");
        return ResponseEntity.ok(levelsService.getLevels());

    }

    @GetMapping("/{id}")
    public ResponseEntity<LevelsResponse> getLevelById(@PathVariable String id){
        log.info("Requested all level with id = {} from database", id);
        return ResponseEntity.ok(levelsService.getLevelById(id));

    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<LevelsResponse> getLevelByCategoryId(@PathVariable String categoryId){
        log.info("Requested all level with categoryId = {} from database", categoryId);
        return ResponseEntity.ok(levelsService.getLevelByCategoryId(categoryId));

    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteLevelById(@PathVariable String id){
        log.info("Requested delete level with id = {} from database", id);

        if(levelsService.deleteLevelById(id)){
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("")
    public ResponseEntity<LevelsResponse> createLevelById(@RequestBody Level level){
        log.info("Requested insert level = {} in database", level);

        return ResponseEntity.ok(levelsService.insertLevel(level));
    }

    @GetMapping("/echo")
    public String echo(@RequestParam String message){
        return "echo "+message;
    }


}
